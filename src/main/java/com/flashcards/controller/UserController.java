package com.flashcards.controller;

import com.flashcards.domain.CustomResponseEntity;
import com.flashcards.dto.LoginDTO;
import com.flashcards.dto.UserInfoDTO;
import com.flashcards.facade.ResetPasswordFacade;
import com.flashcards.facade.UserFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/rest")
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    private static final String LOGIN_ERROR = "Bad credentials.";
    private static final String CREATE_USER_SUCCESS = "User has been created.";
    private static final String CREATE_USER_ERROR = "Could not create the user.";

    @Resource
    private UserFacade userFacade;
    @Resource
    private ResetPasswordFacade resetPasswordFacade;

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody LoginDTO loginDTO) {
        try {
            return CustomResponseEntity.ok(userFacade.authenticate(loginDTO));
        } catch (RuntimeException e) {
            LOG.error(LOGIN_ERROR, e);
            return CustomResponseEntity.error(LOGIN_ERROR);
        }
    }

    @PostMapping(value = "/users")
    public ResponseEntity<?> createUserAsUser(@RequestBody UserInfoDTO userInfoDTO) {
        try {
            userFacade.createUserAsUser(userInfoDTO);
            return CustomResponseEntity.ok(CREATE_USER_SUCCESS);
        } catch (RuntimeException e) {
            LOG.error(LOGIN_ERROR, e);
            return CustomResponseEntity.error(LOGIN_ERROR);
        }
    }

    @PostMapping(value = "/users/privileged")
    @PreAuthorize("hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_ADMIN)")
    public ResponseEntity<?> createUserAsAdmin(@RequestBody UserInfoDTO userInfoDTO) {
        try {
            userFacade.createUser(userInfoDTO);
            resetPasswordFacade.sendResetPasswordEmail(userInfoDTO.getUsername());
            return CustomResponseEntity.ok(CREATE_USER_SUCCESS);
        } catch (RuntimeException e) {
            LOG.error(CREATE_USER_ERROR, e);
            return CustomResponseEntity.error(CREATE_USER_ERROR);
        }
    }
}
