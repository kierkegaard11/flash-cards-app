package com.flashcards.service;

import com.flashcards.domain.RoleEntity;

import java.util.List;

public interface RoleService {

    List<RoleEntity> getAllRoles();

    RoleEntity getRoleByName(String name);
}
