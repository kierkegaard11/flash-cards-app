package com.flashcards.service;

import com.flashcards.domain.PasswordResetTokenEntity;
import com.flashcards.domain.UserEntity;

public interface ResetPasswordService {

    void createPasswordResetTokenForUser(UserEntity userEntity, String token);

    void deletePasswordResetTokenByUserId(int userID);

    PasswordResetTokenEntity getPasswordResetTokenByToken(String token);

    void createResetTokenEmail(UserEntity userEntity, String token);

}
