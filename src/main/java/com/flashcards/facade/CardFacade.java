package com.flashcards.facade;

import com.flashcards.domain.UserEntity;
import com.flashcards.dto.CardDTO;
import com.flashcards.dto.CategoryDTO;

import java.util.List;

public interface CardFacade {

    List<CardDTO> getAllCardsByCategory(int categoryId);

    List<CardDTO> getAllCardsByLevel(int level, UserEntity user);

    CardDTO saveCard(CardDTO cardDTO);

    void deleteCard(int cardId);
}
