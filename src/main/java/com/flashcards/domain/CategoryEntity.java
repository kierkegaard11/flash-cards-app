package com.flashcards.domain;

import javax.persistence.*;

@Entity
@Table(name = "category", schema = "flashcards")
public class CategoryEntity {

    private int categoryId;
    private String name;
    private UserEntity user;

    public CategoryEntity(int categoryId) {
        this.categoryId = categoryId;
    }

    public CategoryEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
