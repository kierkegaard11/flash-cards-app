package com.flashcards.controller;

import com.flashcards.domain.CustomResponseEntity;
import com.flashcards.domain.UserEntity;
import com.flashcards.dto.CardDTO;
import com.flashcards.exception.FlashCardsException;
import com.flashcards.facade.CardFacade;
import com.flashcards.util.CustomSessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/rest")
@PreAuthorize("hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_USER) " +
        "|| hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_EXPERT)")
public class CardController {

    private static final Logger LOG = LoggerFactory.getLogger(CardController.class);
    private static final String SEARCH_CARDS_BY_CATEGORY_ERROR = "Error while getting cards by given category!";
    private static final String SEARCH_CARDS_BY_LEVEL_ERROR = "Error while getting cards by given level!";
    private static final String CREATE_CARD_ERROR = "Error while creating card!";
    private static final String DELETE_CARD_ERROR = "Error while deleting card!";
    private static final String DELETE_CARD_SUCCESS = "Card deleted successfully!";

    @Resource
    private CardFacade cardFacade;

    @GetMapping("/categories/{categoryId}/cards")
    ResponseEntity<?> getCards(@PathVariable int categoryId) {
        try {
            return CustomResponseEntity.ok(cardFacade.getAllCardsByCategory(categoryId));
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(SEARCH_CARDS_BY_CATEGORY_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(SEARCH_CARDS_BY_CATEGORY_ERROR);
        }
    }

    @GetMapping("/cards/{level}")
    ResponseEntity<?> getCardsByLevel(@PathVariable int level) {
        UserEntity user = CustomSessionUtil.getUserFromSession();
        try {
            return CustomResponseEntity.ok(cardFacade.getAllCardsByLevel(level, user));
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(SEARCH_CARDS_BY_LEVEL_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(SEARCH_CARDS_BY_LEVEL_ERROR);
        }
    }

    @PostMapping("/cards")
    ResponseEntity<?> createCard(@RequestBody CardDTO cardDTO) {
        UserEntity user = CustomSessionUtil.getUserFromSession();
        try {
            return CustomResponseEntity.ok(cardFacade.saveCard(cardDTO));
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(CREATE_CARD_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(CREATE_CARD_ERROR);
        }
    }

    @DeleteMapping("/cards/{cardId}")
    ResponseEntity<?> deleteCard(@PathVariable int cardId) {
        try {
            cardFacade.deleteCard(cardId);
            return CustomResponseEntity.ok(DELETE_CARD_SUCCESS);
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(DELETE_CARD_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(DELETE_CARD_ERROR);
        }

    }
}
