package com.flashcards.facade.impl;

import com.flashcards.domain.CardChildAggregateEntity;
import com.flashcards.domain.CardChildAggregateId;
import com.flashcards.domain.CardEntity;
import com.flashcards.domain.ChildEntity;
import com.flashcards.dto.CardDTO;
import com.flashcards.dto.ChosenCategoryDTO;
import com.flashcards.facade.GameFacade;
import com.flashcards.service.CardChildAggregateService;
import com.flashcards.service.CardService;
import com.flashcards.service.ChildService;
import com.flashcards.service.ModelMapperService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class GameFacadeImpl implements GameFacade {

    @Autowired
    private CardService cardService;
    @Autowired
    private ChildService childService;
    @Autowired
    private CardChildAggregateService cardChildAggregateService;
    @Autowired
    private ModelMapperService modelMapperService;

    @Override
    public void saveChosenCategory(ChosenCategoryDTO chosenCategoryDTO) {
        List<CardEntity> cards = cardService.getCardsByCategoryId(chosenCategoryDTO.getCategoryId());
        int childId = chosenCategoryDTO.getChildId();

        for (CardEntity c : cards) {
            CardChildAggregateId cardChildAggregate = new CardChildAggregateId(c.getCardId(), childId);
            cardChildAggregateService.saveCardChildAggregate(cardChildAggregate);
        }
    }

    @Override
    public List<CardDTO> getCardsForGame(int childId) {
        ChildEntity childEntity = childService.getChildById(childId);
        List<CardChildAggregateEntity> chosenCards = cardChildAggregateService.getChosenCardsByChildId(childId)
                .stream()
                .limit(5)
                .collect(toList());

        Collections.shuffle(chosenCards);

        switch (childEntity.getDay()) {

            case 0:
            case 1:
            case 2:
                return firstDayList(chosenCards);
        }
        return null;
    }

    private List<CardDTO> firstDayList(List<CardChildAggregateEntity> chosenCards) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        return chosenCards.stream()
                .map(CardChildAggregateEntity::getCardEntity)
                .map(cardEntity -> modelMapper.map(cardEntity, CardDTO.class))
                .collect(toList());
    }
}
