package com.flashcards.domain;

public interface AppConfigKeys {

    String RESET_PASSWORD_TOKEN_EXPIRATION_DAYS = "resetPasswordTokenExpirationDays";
}
