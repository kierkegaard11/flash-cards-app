package com.flashcards.facade.impl;

import com.flashcards.domain.PasswordResetTokenEntity;
import com.flashcards.domain.UserEntity;
import com.flashcards.exception.passwordResetTokenException.PasswordResetInvalidTokenException;
import com.flashcards.exception.passwordResetTokenException.PasswordResetTokenExpiredException;
import com.flashcards.facade.ResetPasswordFacade;
import com.flashcards.service.PasswordResetTokenService;
import com.flashcards.service.ResetPasswordService;
import com.flashcards.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class ResetPasswordFacadeImpl implements ResetPasswordFacade {

    @Resource
    private UserService userService;
    @Resource
    private PasswordResetTokenService passwordResetTokenService;
    @Resource
    private ResetPasswordService resetPasswordService;

    @Override
    public void sendResetPasswordEmail(String username) {
        UserEntity userEntity = userService.getByUsername(username);
        String token = passwordResetTokenService.createPasswordResetToken();
        resetPasswordService.createPasswordResetTokenForUser(userEntity, token);
        resetPasswordService.createResetTokenEmail(userEntity, token);
    }

    @Override
    public void validatePasswordResetToken(int userID, String token) {
        PasswordResetTokenEntity passwordResetTokenEntity = resetPasswordService.getPasswordResetTokenByToken(token);
        if (passwordResetTokenEntity.getUser().getUserId() != userID) {
            throw new PasswordResetInvalidTokenException();
        }
        if (passwordResetTokenEntity.getExpiryDate().before(new Date())) {
            throw new PasswordResetTokenExpiredException();
        }
    }
}
