package com.flashcards.service;

public interface PasswordResetTokenService {

    String createPasswordResetToken();

}
