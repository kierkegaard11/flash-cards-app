package com.flashcards.controller;

import com.flashcards.domain.CustomResponseEntity;
import com.flashcards.facade.RoleFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/rest/roles")
public class RoleController {

    private static final Logger LOG = LoggerFactory.getLogger(RoleController.class);

    private static final String GET_ALL_ROLES_ERROR = "Could not get all roles.";

    @Resource
    private RoleFacade roleFacade;

    @GetMapping
    @PreAuthorize("hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_ADMIN)")
    public ResponseEntity<?> getAllRoles() {
        try {
            return CustomResponseEntity.ok(roleFacade.getAllRoles());
        } catch (RuntimeException e) {
            LOG.error(GET_ALL_ROLES_ERROR, e);
            return CustomResponseEntity.error(GET_ALL_ROLES_ERROR);
        }
    }
}
