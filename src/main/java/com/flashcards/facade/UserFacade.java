package com.flashcards.facade;

import com.flashcards.dto.JwtResponseDTO;
import com.flashcards.dto.LoginDTO;
import com.flashcards.dto.UserInfoDTO;

public interface UserFacade {

    JwtResponseDTO authenticate(LoginDTO loginDTO);

    void createUser(UserInfoDTO userInfoDTO);

    void createUserAsUser(UserInfoDTO userInfoDTO);

    void changePassword(String token, String newPassword);
}
