package com.flashcards.facade;

import com.flashcards.dto.ChildDTO;
import com.flashcards.dto.ChildInfoDTO;

import java.util.List;

public interface ChildFacade {

    ChildDTO saveChild(ChildInfoDTO childInfoDTO);

    List<ChildDTO> getAllChildren(int userId);

    ChildDTO getChild(int childId);

    ChildDTO updateChild(ChildInfoDTO childInfoDTO, int childId);

    void deleteChild(int childId);
}
