package com.flashcards.service;

import org.modelmapper.PropertyMap;

public interface PropertyMapConfigurationService {

    PropertyMap getConfiguration();

}
