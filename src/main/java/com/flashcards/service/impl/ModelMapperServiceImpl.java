package com.flashcards.service.impl;

import com.flashcards.service.ModelMapperService;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Service;

@Service
public class ModelMapperServiceImpl implements ModelMapperService {

    @Override
    public ModelMapper getConfiguredModelMapper(PropertyMap propertyMap) {
        ModelMapper modelMapper = getModelMapper();
        modelMapper.addMappings(propertyMap);
        return modelMapper;
    }

    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

}
