package com.flashcards.exception.passwordResetTokenException;

public class PasswordResetTokenExpiredException extends RuntimeException {

    public PasswordResetTokenExpiredException() {
        super();
    }

    public PasswordResetTokenExpiredException(String message) {
        super(message);
    }

    public PasswordResetTokenExpiredException(Throwable cause) {
        super(cause);
    }

    public PasswordResetTokenExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

}
