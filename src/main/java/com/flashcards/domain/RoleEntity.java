package com.flashcards.domain;

import javax.persistence.*;

@Entity
@Table(name = "role", schema = "flashcards")
public class RoleEntity {

    private int roleId;
    private String name;

    @Id
    @Column(name = "role_id")
    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
