package com.flashcards.dao;

import com.flashcards.domain.RoleEntity;
import com.flashcards.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserDAO extends JpaRepository<UserEntity, Integer> {

    @Query("SELECT u FROM UserEntity u where u.username = ?1")
    Optional<UserEntity> findByUsername(String username);

    @Modifying
    @Transactional
    @Query("UPDATE UserEntity u SET u.firstName = ?2, u.lastName = ?3, u.username = ?4, u.role = ?5 WHERE u.userId = ?1")
    void updateUser(int userId, String firstName, String lastName, String username, RoleEntity role);

    @Modifying
    @Transactional
    @Query("UPDATE UserEntity u SET u.firstName = ?2, u.lastName = ?3, u.username = ?4, u.password = ?5,  u.role = ?6 WHERE u.userId = ?1")
    void updateUser(int userId, String firstName, String lastName, String username, String password, RoleEntity role);
}
