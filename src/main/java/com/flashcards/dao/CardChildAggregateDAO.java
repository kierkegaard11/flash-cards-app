package com.flashcards.dao;

import com.flashcards.domain.CardChildAggregateEntity;
import com.flashcards.domain.CardChildAggregateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardChildAggregateDAO extends JpaRepository<CardChildAggregateEntity, CardChildAggregateId> {

    @Query("SELECT  c FROM CardChildAggregateEntity c WHERE c.cardEntity.category.categoryId = ?1")
    List<CardChildAggregateEntity> getByCategory(int categoryId);

    @Query("SELECT c FROM CardChildAggregateEntity c WHERE c.childEntity.childId=?1")
    List<CardChildAggregateEntity> getByChildId(int childId);

}
