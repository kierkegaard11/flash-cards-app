package com.flashcards.exception.configException;

public class AppConfigNotFoundException extends ConfigException {

    public AppConfigNotFoundException() {
        super();
    }

    public AppConfigNotFoundException(String message) {
        super(message);
    }

    public AppConfigNotFoundException(Throwable cause) {
        super(cause);
    }

    public AppConfigNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
