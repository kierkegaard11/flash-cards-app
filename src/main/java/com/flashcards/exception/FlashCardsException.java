package com.flashcards.exception;

public class FlashCardsException extends RuntimeException {

    public FlashCardsException() {
        super();
    }

    public FlashCardsException(String message) {
        super(message);
    }

    public FlashCardsException(Throwable cause) {
        super(cause);
    }

    public FlashCardsException(String message, Throwable cause) {
        super(message, cause);
    }
}
