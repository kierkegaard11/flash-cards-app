package com.flashcards.dao;

import com.flashcards.domain.CategoryEntity;
import com.flashcards.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CategoryDAO extends JpaRepository<CategoryEntity, Integer> {

    @Query("SELECT c FROM CategoryEntity c WHERE c.name LIKE %?1% AND c.user=?2")
    List<CategoryEntity> findAllByParameters(String searchCriteria, UserEntity userEntity);

    @Query("SELECT c FROM CategoryEntity c WHERE c.user=?1")
    List<CategoryEntity> findAllByUserId(UserEntity userEntity);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("UPDATE CategoryEntity c SET c.name=?1 WHERE c.categoryId=?2")
    void updateCategory(String name, int categoryId);
}
