package com.flashcards.facade.impl;

import com.flashcards.domain.ChildEntity;
import com.flashcards.domain.UserEntity;
import com.flashcards.dto.ChildDTO;
import com.flashcards.dto.ChildInfoDTO;
import com.flashcards.facade.ChildFacade;
import com.flashcards.service.ChildService;
import com.flashcards.service.ModelMapperService;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ChildFacadeImpl implements ChildFacade {

    @Resource
    private ModelMapperService modelMapperService;
    @Resource
    private ChildService childService;

    @Override
    public ChildDTO saveChild(ChildInfoDTO childInfoDTO) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        modelMapper.addMappings(new PropertyMap<ChildInfoDTO, ChildEntity>() {
            @Override
            protected void configure() {
                skip(destination.getUser());
            }
        });
        ChildEntity mappedChild = modelMapper.map(childInfoDTO, ChildEntity.class);
        UserEntity user = new UserEntity();
        user.setUserId(childInfoDTO.getUserId());
        mappedChild.setUser(user);
        ChildEntity child = childService.saveChild(mappedChild);
        child.setDay(1);
        return modelMapper.map(child, ChildDTO.class);
    }

    @Override
    public List<ChildDTO> getAllChildren(int userId) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        return childService.getAllChildren(userId)
                .stream()
                .map(childEntity -> modelMapper.map(childEntity, ChildDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ChildDTO getChild(int childId) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        return modelMapper.map(childService.getChildById(childId), ChildDTO.class);
    }

    @Override
    public ChildDTO updateChild(ChildInfoDTO childInfoDTO, int childId) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        modelMapper.addMappings(new PropertyMap<ChildInfoDTO, ChildEntity>() {
            @Override
            protected void configure() {
                skip(destination.getUser());
            }
        });
        ChildEntity mappedChild = modelMapper.map(childInfoDTO, ChildEntity.class);
        UserEntity user = new UserEntity();
        user.setUserId(childInfoDTO.getUserId());
        mappedChild.setUser(user);
        return modelMapper.map(childService.updateChild(mappedChild), ChildDTO.class);
    }

    @Override
    public void deleteChild(int childId) {
        childService.deleteChild(childId);
    }
}
