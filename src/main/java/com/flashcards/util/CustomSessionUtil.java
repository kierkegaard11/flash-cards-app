package com.flashcards.util;

import com.flashcards.domain.UserEntity;
import com.flashcards.security.services.UserPrinciple;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class CustomSessionUtil {

    private CustomSessionUtil() {
    }

    public static UserEntity getUserFromSession() {
        return ((UserPrinciple) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()).getUserEntity();
    }

    public static List<String> getUsersRolesFromSession() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .map(p -> (UserPrinciple) p)
                .map(UserPrinciple::getAuthorities)
                .map(grantedAuthorities -> (Collection<SimpleGrantedAuthority>) grantedAuthorities)
                .orElse(Collections.emptyList())
                .stream()
                .map(SimpleGrantedAuthority::getAuthority)
                .collect(Collectors.toList());
    }
}