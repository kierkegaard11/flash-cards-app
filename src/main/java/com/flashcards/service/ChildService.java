package com.flashcards.service;

import com.flashcards.domain.ChildEntity;

import java.util.List;

public interface ChildService {

    ChildEntity saveChild(ChildEntity childEntity);

    List<ChildEntity> getAllChildren(int userId);

    ChildEntity getChildById(int childId);

    void deleteChild(int childId);

    ChildEntity updateChild(ChildEntity mappedChild);
}
