package com.flashcards.service.impl;

import com.flashcards.dao.ChildDAO;
import com.flashcards.domain.ChildEntity;
import com.flashcards.exception.FlashCardsException;
import com.flashcards.service.ChildService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ChildServiceImpl implements ChildService {

    private static final String CHILD_NOT_FOUND_FOR_THE_GIVEN_ID = "Child not found for the given id";

    @Resource
    private ChildDAO childDAO;

    @Override
    public ChildEntity saveChild(ChildEntity childEntity) {
        return childDAO.save(childEntity);
    }

    @Override
    public List<ChildEntity> getAllChildren(int userId) {
        return childDAO.findChildrenByUserId(userId);
    }

    @Override
    public ChildEntity getChildById(int childId) {
        return childDAO.findById(childId)
                .orElseThrow(() -> new RuntimeException(CHILD_NOT_FOUND_FOR_THE_GIVEN_ID));
    }

    @Override
    public void deleteChild(int childId) {
        if (childDAO.findById(childId)
                .isPresent()) {
            childDAO.deleteById(childId);
        } else {
            throw new FlashCardsException(CHILD_NOT_FOUND_FOR_THE_GIVEN_ID);
        }
    }

    @Override
    public ChildEntity updateChild(ChildEntity child) {
        if (childDAO.findById(child.getChildId()).isPresent()) {
            childDAO.updateChild(child.getFirstName(), child.getLastName(), child.getBirthDate(), child.getChildId());
            return childDAO.findById(child.getChildId())
                    .orElseThrow(() -> new FlashCardsException(CHILD_NOT_FOUND_FOR_THE_GIVEN_ID));
        } else {
            throw new FlashCardsException(CHILD_NOT_FOUND_FOR_THE_GIVEN_ID);
        }
    }
}
