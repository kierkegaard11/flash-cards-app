package com.flashcards.service.impl;

import com.flashcards.controller.ResetPasswordController;
import com.flashcards.dao.ResetPasswordDAO;
import com.flashcards.domain.AppConfigEntity;
import com.flashcards.domain.AppConfigKeys;
import com.flashcards.domain.PasswordResetTokenEntity;
import com.flashcards.domain.UserEntity;
import com.flashcards.exception.passwordResetTokenException.PasswordResetInvalidTokenException;
import com.flashcards.service.AppConfigService;
import com.flashcards.service.ResetPasswordService;
import com.flashcards.util.CustomDateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Optional;

@Service
public class ResetPasswordServiceImpl implements ResetPasswordService {

    private static final Logger LOG  = LoggerFactory.getLogger(ResetPasswordServiceImpl.class);

    private static final String RESET_PASSWORD_LINK_PATTERN = "%s%s?userID=%s&token=%s";

    @Value("${spring.mail.username}")
    protected String springMailUsername;
    @Value("${application.base.url}")
    protected String applicationBaseUrl;

    @Resource
    private AppConfigService appConfigService;
    @Resource
    private JavaMailSender mailSender;
    @Resource
    private ResetPasswordDAO resetPasswordDAO;

    @Override
    public void createPasswordResetTokenForUser(UserEntity userEntity, String token) {
        Optional<PasswordResetTokenEntity> passwordResetTokenEntity = resetPasswordDAO.findByUserId(userEntity.getUserId());
        if (passwordResetTokenEntity.isPresent()) {
            updatePasswordResetToken(passwordResetTokenEntity.get(), token);
        } else {
            savePasswordResetToken(userEntity, token);
        }
    }

    @Override
    public void deletePasswordResetTokenByUserId(int userID) {
        if (resetPasswordDAO.findByUserId(userID).isPresent()) {
            resetPasswordDAO.deleteByUserId(userID);
        }
    }

    @Override
    public PasswordResetTokenEntity getPasswordResetTokenByToken(String token) {
        return resetPasswordDAO.findByToken(token).orElseThrow(PasswordResetInvalidTokenException::new);
    }

    @Async
    @Override
    public void createResetTokenEmail(UserEntity userEntity, String token) {
        try {
            SimpleMailMessage email = new SimpleMailMessage();
            String body = "Please follow this link to reset your password:\n" + getResetPasswordLink(userEntity.getUserId(), token);
            email.setSubject("Reset Password");
            email.setText(body);
            email.setTo(userEntity.getUsername());
            email.setFrom(springMailUsername);
            mailSender.send(email);
        } catch (RuntimeException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private void updatePasswordResetToken(PasswordResetTokenEntity passwordResetTokenEntity, String token) {
        passwordResetTokenEntity.setToken(token);
        passwordResetTokenEntity.setExpiryDate(getExpiryDate());
        resetPasswordDAO.update(
                passwordResetTokenEntity.getPasswordResetTokenId(),
                passwordResetTokenEntity.getToken(),
                passwordResetTokenEntity.getExpiryDate()
        );
    }

    private void savePasswordResetToken(UserEntity userEntity, String token) {
        PasswordResetTokenEntity passwordResetTokenEntity = new PasswordResetTokenEntity();
        passwordResetTokenEntity.setUser(userEntity);
        passwordResetTokenEntity.setToken(token);
        passwordResetTokenEntity.setExpiryDate(getExpiryDate());
        resetPasswordDAO.save(passwordResetTokenEntity);
    }

    private Date getExpiryDate() {
        return CustomDateUtil.getTodayPlusDaysToAdd(getResetPasswordTokenExpirationDays());
    }

    private int getResetPasswordTokenExpirationDays() {
        AppConfigEntity appConfigEntity = appConfigService.getAppConfigByKey(AppConfigKeys.RESET_PASSWORD_TOKEN_EXPIRATION_DAYS);
        try {
            return Integer.parseInt(appConfigEntity.getValue());
        } catch (Exception e) {
            LOG.error("Invalid " + AppConfigKeys.RESET_PASSWORD_TOKEN_EXPIRATION_DAYS + " value, could not parse.", e);
            return Integer.parseInt(appConfigEntity.getDefaultValue());
        }
    }

    private String getResetPasswordLink(int userId, String token) {
        return String.format(
                RESET_PASSWORD_LINK_PATTERN,
                applicationBaseUrl,
                ResetPasswordController.RESET_PASSWORD_PAGE_URL, // TODO - get rid off this
                userId,
                token
        );
    }
}
