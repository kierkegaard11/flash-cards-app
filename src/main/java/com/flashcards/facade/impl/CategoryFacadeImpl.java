package com.flashcards.facade.impl;

import com.flashcards.domain.CategoryEntity;
import com.flashcards.dto.CategoryDTO;
import com.flashcards.facade.CategoryFacade;
import com.flashcards.service.CategoryService;
import com.flashcards.service.ModelMapperService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryFacadeImpl implements CategoryFacade {

    @Resource
    ModelMapperService modelMapperService;
    @Resource
    CategoryService categoryService;

    @Override
    public List<CategoryDTO> getAllCategories(int userId) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        return categoryService.getAllCategories(userId)
                .stream()
                .map(categoryEntity -> modelMapper.map(categoryEntity, CategoryDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CategoryDTO> searchCategories(String searchCriteria, int userId) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        return categoryService.searchCategories(searchCriteria, userId)
                .stream()
                .map(categoryEntity -> modelMapper.map(categoryEntity, CategoryDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDTO saveCategory(CategoryDTO categoryDTO) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        CategoryEntity mappedCategory = modelMapper.map(categoryDTO, CategoryEntity.class);
        CategoryEntity categoryEntity = categoryService.saveCategory(mappedCategory);
        return modelMapper.map(categoryEntity, CategoryDTO.class);
    }

    @Override
    public CategoryDTO updateCategory(CategoryDTO categoryDTO, int categoryId) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        CategoryEntity mappedCategory = modelMapper.map(categoryDTO, CategoryEntity.class);
        CategoryEntity categoryEntity = categoryService.updateCategory(mappedCategory, categoryId);
        return modelMapper.map(categoryEntity, CategoryDTO.class);
    }

    @Override
    public void deleteCategory(int categoryId) {
        categoryService.deleteCategory(categoryId);
    }
}
