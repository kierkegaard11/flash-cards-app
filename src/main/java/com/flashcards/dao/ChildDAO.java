package com.flashcards.dao;

import com.flashcards.domain.ChildEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ChildDAO extends JpaRepository<ChildEntity, Integer> {

    @Query("SELECT c FROM ChildEntity c WHERE c.user.userId = ?1")
    List<ChildEntity> findChildrenByUserId(int userId);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("UPDATE ChildEntity c SET c.firstName=?1, c.lastName=?2, c.birthDate=?3 WHERE c.childId=?4")
    void updateChild(String firstName, String lastName, LocalDate birthDate, int childId);
}
