package com.flashcards.exception.passwordResetTokenException;

public class PasswordResetInvalidTokenException extends RuntimeException {

    public PasswordResetInvalidTokenException() {
        super();
    }

    public PasswordResetInvalidTokenException(String message) {
        super(message);
    }

    public PasswordResetInvalidTokenException(Throwable cause) {
        super(cause);
    }

    public PasswordResetInvalidTokenException(String message, Throwable cause) {
        super(message, cause);
    }

}
