package com.flashcards.facade.impl;

import com.flashcards.dto.RoleDTO;
import com.flashcards.facade.RoleFacade;
import com.flashcards.service.ModelMapperService;
import com.flashcards.service.RoleService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class RoleFacadeImpl implements RoleFacade {

    @Resource
    private RoleService roleService;
    @Resource
    private ModelMapperService modelMapperService;

    @Override
    public List<RoleDTO> getAllRoles() {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        return roleService.getAllRoles()
                .stream()
                .map(roleEntity -> modelMapper.map(roleEntity, RoleDTO.class))
                .collect(toList());
    }
}
