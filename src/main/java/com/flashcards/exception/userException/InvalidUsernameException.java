package com.flashcards.exception.userException;

public class InvalidUsernameException extends UserException {

    public InvalidUsernameException() {
        super();
    }

    public InvalidUsernameException(String message) {
        super(message);
    }

    public InvalidUsernameException(Throwable cause) {
        super(cause);
    }

    public InvalidUsernameException(String message, Throwable cause) {
        super(message, cause);
    }

}
