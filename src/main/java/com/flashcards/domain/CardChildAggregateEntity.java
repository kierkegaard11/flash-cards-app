package com.flashcards.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "card_child_aggregate", schema = "flashcards")
public class CardChildAggregateEntity {

    @EmbeddedId
    private CardChildAggregateId cardChildAggregateId;

    @ManyToOne
    @MapsId("card_id")
    @JoinColumn(name = "card_id")
    private CardEntity cardEntity;

    @ManyToOne
    @MapsId("child_id")
    @JoinColumn(name = "child_id")
    private ChildEntity childEntity;

    @Basic
    @Column(name = "state")
    private Integer state;

    public CardChildAggregateEntity() {
    }

    public CardChildAggregateEntity(CardChildAggregateId cardChildAggregateId) {
        this.cardChildAggregateId = cardChildAggregateId;
    }

    public CardChildAggregateId getCardChildAggregateId() {
        return cardChildAggregateId;
    }

    public void setCardChildAggregateId(CardChildAggregateId cardChildAggregateId) {
        this.cardChildAggregateId = cardChildAggregateId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public CardEntity getCardEntity() {
        return cardEntity;
    }

    public void setCardEntity(CardEntity cardEntity) {
        this.cardEntity = cardEntity;
    }

    public ChildEntity getChildEntity() {
        return childEntity;
    }

    public void setChildEntity(ChildEntity childEntity) {
        this.childEntity = childEntity;
    }

    @Override
    public String toString() {
        return "CardChildAggregateEntity{" +
                "cardChildAggregateId=" + cardChildAggregateId +
                ", state=" + state +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardChildAggregateEntity that = (CardChildAggregateEntity) o;
        return Objects.equals(cardChildAggregateId, that.cardChildAggregateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardChildAggregateId);
    }
}
