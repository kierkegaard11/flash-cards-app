package com.flashcards.service;

import com.flashcards.domain.CategoryEntity;

import java.util.List;

public interface CategoryService {

    public List<CategoryEntity> getAllCategories(int userId);

    public List<CategoryEntity> searchCategories(String searchCriteria, int userId);

    CategoryEntity saveCategory(CategoryEntity category);

    CategoryEntity updateCategory(CategoryEntity category, int categoryId);

    void deleteCategory(int categoryId);
}
