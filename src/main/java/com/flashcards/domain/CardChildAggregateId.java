package com.flashcards.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CardChildAggregateId implements Serializable {

    @Column(name = "card_id")
    private int cardId;

    @Column(name = "child_id")
    private int childId;

    public CardChildAggregateId() {
    }

    public CardChildAggregateId(int cardId, int childId) {
        this.cardId = cardId;
        this.childId = childId;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    @Override
    public String toString() {
        return "CardChildAggregateId{" +
                "cardId=" + cardId +
                ", childId=" + childId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardChildAggregateId that = (CardChildAggregateId) o;
        return cardId == that.cardId &&
                childId == that.childId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardId, childId);
    }
}
