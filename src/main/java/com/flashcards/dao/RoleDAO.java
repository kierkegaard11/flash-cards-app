package com.flashcards.dao;

import com.flashcards.domain.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleDAO extends JpaRepository<RoleEntity, Integer> {

    @Query("SELECT r FROM RoleEntity r where r.name = ?1")
    Optional<RoleEntity> findByName(String name);
}
