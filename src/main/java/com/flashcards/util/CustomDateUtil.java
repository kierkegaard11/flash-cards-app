package com.flashcards.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public final class CustomDateUtil {

    private CustomDateUtil() {
    }

    public static LocalDate getLocalDateFromDate(Date date) {
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static Date getDateFromLocalDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public static Date getTodayPlusDaysToAdd(int daysToAdd) {
        return getDateFromLocalDate(LocalDate.now().plusDays(daysToAdd));
    }

    public static ZonedDateTime convertStringToZonedDateTime(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").ISO_LOCAL_DATE;
        LocalDate localDate = LocalDate.parse(date, formatter);
        return localDate.atStartOfDay(ZoneId.systemDefault());
    }
}
