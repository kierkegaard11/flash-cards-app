package com.flashcards.facade.impl;

import com.flashcards.domain.CardEntity;
import com.flashcards.domain.UserEntity;
import com.flashcards.dto.CardDTO;
import com.flashcards.facade.CardFacade;
import com.flashcards.service.CardChildAggregateService;
import com.flashcards.service.CardService;
import com.flashcards.service.ModelMapperService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class CardFacadeImpl implements CardFacade {
    @Resource
    private CardService cardService;
    @Resource
    private ModelMapperService modelMapperService;
    @Resource
    private CardChildAggregateService cardChildAggregateService;

    @Override
    public List<CardDTO> getAllCardsByCategory(int categoryId) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        return cardService.getCardsByCategoryId(categoryId)
                .stream()
                .map(cardEntity -> modelMapper.map(cardEntity, CardDTO.class))
                .collect(toList());
    }

    @Override
    public List<CardDTO> getAllCardsByLevel(int level, UserEntity user) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        return cardService.getCardsByLevel(level, user)
                .stream()
                .map(cardEntity -> modelMapper.map(cardEntity, CardDTO.class))
                .collect(toList());
    }

    @Override
    public CardDTO saveCard(CardDTO cardDTO) {
        ModelMapper modelMapper = modelMapperService.getModelMapper();
        CardEntity mappedCard = modelMapper.map(cardDTO, CardEntity.class);
        CardEntity cardEntity = cardService.saveCard(mappedCard);

        /*if (cardDTO.getCategory() != null) {
            saveCardChildAggregate(cardEntity);
        }*/
        return modelMapper.map(cardEntity, CardDTO.class);
    }

    private void saveCardChildAggregate(CardEntity cardEntity) {
        cardChildAggregateService.addNewAggregate(cardEntity.getCardId(), cardEntity.getCategory());
    }

    @Override
    public void deleteCard(int cardId) {
        cardService.deleteCard(cardId);
    }
}
