package com.flashcards.service.impl.propertyMapConfiguration;

import com.flashcards.domain.UserEntity;
import com.flashcards.dto.UserInfoDTO;
import com.flashcards.service.PropertyMapConfigurationService;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Service;

@Service
public class PropertyMapConfigurationRegisterServiceImpl implements PropertyMapConfigurationService {

    @Override
    public PropertyMap getConfiguration() {
        return new PropertyMap<UserInfoDTO, UserEntity>() {
            protected void configure() {
                map().setPassword(source.getPassword()); // TODO - check if it is obsolete
            }
        };
    }
}
