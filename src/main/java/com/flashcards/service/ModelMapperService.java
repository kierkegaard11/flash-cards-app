package com.flashcards.service;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

public interface ModelMapperService {

    ModelMapper getConfiguredModelMapper(PropertyMap propertyMap);

    ModelMapper getModelMapper();
}
