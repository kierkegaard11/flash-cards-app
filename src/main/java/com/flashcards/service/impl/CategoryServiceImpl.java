package com.flashcards.service.impl;

import com.flashcards.dao.CategoryDAO;
import com.flashcards.domain.CategoryEntity;
import com.flashcards.domain.UserEntity;
import com.flashcards.exception.FlashCardsException;
import com.flashcards.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static final String CATEGORY_NOT_FOUND_FOR_THE_GIVEN_ID = "Category not found for the given id";
    @Resource
    private CategoryDAO categoryDAO;

    @Override
    public List<CategoryEntity> getAllCategories(int userId) {
        UserEntity user = new UserEntity();
        user.setUserId(userId);
        return categoryDAO.findAllByUserId(user);
    }

    @Override
    public List<CategoryEntity> searchCategories(String searchCriteria, int userId) {
        UserEntity user = new UserEntity();
        user.setUserId(userId);
        return categoryDAO.findAllByParameters(searchCriteria, user);
    }

    @Override
    public CategoryEntity saveCategory(CategoryEntity category) {
        return categoryDAO.save(category);
    }

    @Transactional
    @Override
    public CategoryEntity updateCategory(CategoryEntity category, int categoryId) {
        if (categoryDAO.findById(categoryId).isPresent()) {
            categoryDAO.updateCategory(category.getName(), categoryId);
            return categoryDAO.findById(categoryId)
                    .orElseThrow(() ->new FlashCardsException(CATEGORY_NOT_FOUND_FOR_THE_GIVEN_ID));
        } else {
            throw new FlashCardsException(CATEGORY_NOT_FOUND_FOR_THE_GIVEN_ID);
        }
    }

    @Override
    public void deleteCategory(int categoryId) {
        if (categoryDAO.findById(categoryId).isPresent()) {
            categoryDAO.deleteById(categoryId);
        } else {
            throw new FlashCardsException(CATEGORY_NOT_FOUND_FOR_THE_GIVEN_ID);
        }
    }
}
