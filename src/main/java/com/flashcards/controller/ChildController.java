package com.flashcards.controller;

import com.flashcards.controller.validator.ChildAccessValidator;
import com.flashcards.domain.CustomResponseEntity;
import com.flashcards.dto.ChildInfoDTO;
import com.flashcards.exception.FlashCardsException;
import com.flashcards.facade.ChildFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@PreAuthorize("hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_USER) " +
        "|| hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_EXPERT)")
@RequestMapping("/rest/children")
public class ChildController {

    private static final Logger LOG = LoggerFactory.getLogger(ChildController.class);

    private static final String NEW_CHILD_ERROR = "Cannot create new child!";
    private static final String SEARCH_CHILDREN_ERROR = "Cannot retrieve children!";
    public static final String ERROR_WHILE_UPDATING_CHILD = "Error while updating child";
    public static final String CHILD_DELETED_SUCCESSFULLY = "Child deleted successfully";
    public static final String ERROR_WHILE_DELETING_CHILD = "Error while deleting child";

    @Resource
    private ChildFacade childFacade;
    @Resource
    private ChildAccessValidator childAccessValidator;

    @PostMapping
    public ResponseEntity<?> createChild(@RequestBody ChildInfoDTO childInfoDTO) {
        try {
            childAccessValidator.isUserAccessingHisResources(childInfoDTO.getUserId());
            return CustomResponseEntity.ok(childFacade.saveChild(childInfoDTO));
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(NEW_CHILD_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(NEW_CHILD_ERROR);
        }
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> getChildren(@PathVariable int userId) {
        try {
            childAccessValidator.isUserAccessingHisResources(userId);
            return CustomResponseEntity.ok(childFacade.getAllChildren(userId));
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(SEARCH_CHILDREN_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(SEARCH_CHILDREN_ERROR);
        }
    }

    @GetMapping("/{childId}")
    public ResponseEntity<?> getChild(@PathVariable int childId) {
        try {
            childAccessValidator.isUserAccessingHisChild(childId);
            return CustomResponseEntity.ok(childFacade.getChild(childId));
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(SEARCH_CHILDREN_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(SEARCH_CHILDREN_ERROR);
        }
    }

    @PutMapping("/{childId}")
    public ResponseEntity<?> updateChild(@RequestBody ChildInfoDTO childInfoDTO, @PathVariable int childId) {
        try {
            childAccessValidator.isUserAccessingHisChild(childId);
            return CustomResponseEntity.ok(childFacade.updateChild(childInfoDTO, childId));
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(ERROR_WHILE_UPDATING_CHILD);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ERROR_WHILE_UPDATING_CHILD);
        }
    }

    @DeleteMapping("/{childId}")
    public ResponseEntity<?> deleteChild(@PathVariable int childId) {
        try {
            childFacade.deleteChild(childId);
            return CustomResponseEntity.ok(CHILD_DELETED_SUCCESSFULLY);
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(ERROR_WHILE_DELETING_CHILD);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ERROR_WHILE_DELETING_CHILD);
        }
    }
}
