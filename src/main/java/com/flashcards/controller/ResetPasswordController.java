package com.flashcards.controller;

import com.flashcards.domain.CustomResponseEntity;
import com.flashcards.dto.ForgotPasswordDTO;
import com.flashcards.dto.ResetPasswordDTO;
import com.flashcards.exception.passwordResetTokenException.PasswordResetInvalidTokenException;
import com.flashcards.exception.passwordResetTokenException.PasswordResetTokenExpiredException;
import com.flashcards.facade.ResetPasswordFacade;
import com.flashcards.facade.UserFacade;
import com.flashcards.security.jwt.JwtProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Controller
public class ResetPasswordController {

    private static final Logger LOG = LoggerFactory.getLogger(ResetPasswordController.class);

    public static final String RESET_PASSWORD_PAGE_URL = "/reset-password-page";
    private static final String MODEL_ATTR_MESSAGE = "message";
    private static final String MODEL_ATTR_ERROR = "error";
    private static final String MODEL_ATTR_RESET_PASSWORD_FORM = "resetPasswordForm";
    private static final String RESET_PASSWORD_PAGE = "reset-password";

    @Value("${message.validate.password.reset.token.error}")
    private String messageValidatePasswordResetTokenError;
    @Value("${message.token.invalid}")
    private String messageTokenInvalid;
    @Value("${message.token.expired}")
    private String messageTokenExpired;
    @Value("${message.forgot.password.send.email.success}")
    private String messageForgotPasswordSendEmailSuccess;
    @Value("${message.forgot.password.send.email.error}")
    private String messageForgotPasswordSendEmailError;
    @Value("${message.reset.password.success}")
    private String messageResetPasswordSuccess;
    @Value("${message.reset.password.error}")
    private String messageResetPasswordError;

    @Resource
    private ResetPasswordFacade resetPasswordFacade;
    @Resource
    private UserFacade userFacade;
    @Resource
    private JwtProvider tokenProvider;

    @PostMapping(value = "/rest/reset-password-email")
    @ResponseBody
    ResponseEntity<?> sendResetPasswordEmail(@RequestBody ForgotPasswordDTO forgotPasswordDTO) {
        try {
            resetPasswordFacade.sendResetPasswordEmail(forgotPasswordDTO.getUsername());
            return CustomResponseEntity.ok(messageForgotPasswordSendEmailSuccess);
        } catch (UsernameNotFoundException e) {
            LOG.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage());
        } catch (RuntimeException e) {
            LOG.error(messageForgotPasswordSendEmailError, e);
            return CustomResponseEntity.error(messageForgotPasswordSendEmailError);
        }
    }

    @GetMapping(value = RESET_PASSWORD_PAGE_URL)
    String resetPasswordPage(@RequestParam int userID, @RequestParam String token, Model model) {
        try {
            resetPasswordFacade.validatePasswordResetToken(userID, token);
            ResetPasswordDTO resetPasswordDTO = new ResetPasswordDTO();
            resetPasswordDTO.setToken(token);
            model.addAttribute(MODEL_ATTR_RESET_PASSWORD_FORM, resetPasswordDTO);
        } catch (PasswordResetInvalidTokenException e) {
            LOG.error(messageTokenInvalid, e);
            model.addAttribute(MODEL_ATTR_ERROR, messageTokenInvalid);
        } catch (PasswordResetTokenExpiredException e) {
            LOG.error(messageTokenExpired, e);
            model.addAttribute(MODEL_ATTR_ERROR, messageTokenExpired);
        } catch (RuntimeException e) {
            LOG.error(messageValidatePasswordResetTokenError, e);
            model.addAttribute(MODEL_ATTR_ERROR, messageValidatePasswordResetTokenError);
        }
        return RESET_PASSWORD_PAGE;
    }

    @PostMapping(value = "/reset-password")
    public String resetPassword(@ModelAttribute ResetPasswordDTO resetPasswordDTO, Model model) {
        try {
            userFacade.changePassword(resetPasswordDTO.getToken(), resetPasswordDTO.getNewPassword());
            model.addAttribute(MODEL_ATTR_MESSAGE, messageResetPasswordSuccess);
        } catch (RuntimeException e) {
            LOG.error(messageResetPasswordError, e);
            model.addAttribute(MODEL_ATTR_ERROR, messageResetPasswordError);
        }
        return RESET_PASSWORD_PAGE;
    }

}
