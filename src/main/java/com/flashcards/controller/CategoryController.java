package com.flashcards.controller;

import com.flashcards.domain.CustomResponseEntity;
import com.flashcards.dto.CategoryDTO;
import com.flashcards.exception.FlashCardsException;
import com.flashcards.facade.CategoryFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.xml.ws.Response;
import java.util.List;

@RestController
@RequestMapping("/rest/user/{userId}/categories")
@PreAuthorize("hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_USER) " +
        "|| hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_EXPERT)")
public class CategoryController {

    private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);

    private static final String SEARCH_CATEGORIES_ERROR = "Cannot retreive categories!";
    private static final String CREATE_CATEGORY_ERROR = "Error while creating category";
    private static final String UPDATE_CATEGORY_ERROR = "Error while updating category";
    private static final String DELETE_CATEGORY_ERROR = "Error while deleting category";
    private static final String DELETE_CATEGORY_SUCCESS = "Category deleted successfully";

    @Resource
    private CategoryFacade categoryFacade;

    @GetMapping
    public ResponseEntity<?> getCategories(@PathVariable int userId) {
        try {
            // TODO - add validator for user access
            List<CategoryDTO> categories = categoryFacade.getAllCategories(userId);
            return CustomResponseEntity.ok(categories);
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(SEARCH_CATEGORIES_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(SEARCH_CATEGORIES_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<?> createCategory(@PathVariable int userId,
                                            @RequestBody CategoryDTO categoryDTO) {
        try {
            // TODO - add validator for user access
            CategoryDTO category = categoryFacade.saveCategory(categoryDTO);
            return CustomResponseEntity.ok(category);
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(CREATE_CATEGORY_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(CREATE_CATEGORY_ERROR);
        }
    }

    @PutMapping("/{categoryId}")
    public ResponseEntity<?> updateCategory(@PathVariable int userId,
                                            @PathVariable int categoryId,
                                            @RequestBody CategoryDTO categoryDTO){
        try {
            // TODO - add validator for user access
            CategoryDTO category = categoryFacade.updateCategory(categoryDTO, categoryId);
            return CustomResponseEntity.ok(category);
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(UPDATE_CATEGORY_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(UPDATE_CATEGORY_ERROR);
        }
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<?> deleteCategory(@PathVariable int userId,
                                            @PathVariable int categoryId){
        try {
            categoryFacade.deleteCategory(categoryId);
            return CustomResponseEntity.ok(DELETE_CATEGORY_SUCCESS);
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(DELETE_CATEGORY_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(DELETE_CATEGORY_ERROR);
        }
    }
}
