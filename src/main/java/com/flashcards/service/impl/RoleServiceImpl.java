package com.flashcards.service.impl;

import com.flashcards.dao.RoleDAO;
import com.flashcards.domain.RoleEntity;
import com.flashcards.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class RoleServiceImpl implements RoleService {

    private static final String ROLE_NOT_FOUND_BY_NAME =
            "Role with name %s does not exist";

    @Resource
    private RoleDAO roleDAO;

    @Override
    public List<RoleEntity> getAllRoles() {
        return roleDAO.findAll();
    }

    @Override
    public RoleEntity getRoleByName(String name) {
        return roleDAO.findByName(name)
                .orElseThrow(() -> new NoSuchElementException(String.format(ROLE_NOT_FOUND_BY_NAME, name)));
    }
}
