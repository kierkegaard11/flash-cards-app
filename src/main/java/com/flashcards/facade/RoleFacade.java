package com.flashcards.facade;

import com.flashcards.dto.RoleDTO;

import java.util.List;

public interface RoleFacade {

    List<RoleDTO> getAllRoles();
}
