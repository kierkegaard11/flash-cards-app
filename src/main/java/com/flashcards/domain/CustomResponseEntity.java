package com.flashcards.domain;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class CustomResponseEntity {

    private CustomResponseEntity() {
    }

    public static ResponseEntity<?> ok(Object body) {
        return ResponseEntity.ok(body);
    }

    public static ResponseEntity<?> ok(String message) {
        return ResponseEntity.ok(new CustomResponse(message));
    }

    public static ResponseEntity<?> error(String message) {
        return new ResponseEntity<>(new CustomResponse(message), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
