package com.flashcards.service;

import com.flashcards.domain.AppConfigEntity;

import java.util.List;

public interface AppConfigService {

    AppConfigEntity getAppConfigByKey(String key);

    List<AppConfigEntity> getAllAppConfig();

    void updateAppConfig(AppConfigEntity appConfigEntity);
}
