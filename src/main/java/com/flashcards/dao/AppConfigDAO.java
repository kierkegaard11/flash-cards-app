package com.flashcards.dao;

import com.flashcards.domain.AppConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AppConfigDAO extends JpaRepository<AppConfigEntity, String> {

    @Transactional
    @Modifying
    @Query("UPDATE AppConfigEntity t SET t.value = ?2 WHERE t.key = ?1")
    void updateConfig(String key, String value);
}
