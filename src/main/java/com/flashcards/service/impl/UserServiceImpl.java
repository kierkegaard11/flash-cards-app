package com.flashcards.service.impl;

import com.flashcards.dao.UserDAO;
import com.flashcards.domain.JwtResponse;
import com.flashcards.domain.RoleEnum;
import com.flashcards.domain.UserEntity;
import com.flashcards.exception.FlashCardsException;
import com.flashcards.exception.userException.InvalidUsernameException;
import com.flashcards.exception.userException.UserNotFoundException;
import com.flashcards.security.jwt.JwtProvider;
import com.flashcards.security.services.UserPrinciple;
import com.flashcards.service.RoleService;
import com.flashcards.service.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final String USER_WITH_USERNAME_NOT_FOUND =
            "User with a username %s does not exist.";
    private static final String USERNAME_ALREADY_IN_USE =
            "Cannot save the user %s: username %s is already in use!";
    private static final String USER_NOT_FOUND_BY_ID =
            "User with an id %s does not exist.";
    private static final String CANNOT_UPDATE_USER_NOT_FOUND_BY_ID =
            "Cannot update the user (%s): " + USER_NOT_FOUND_BY_ID;

    @Resource
    private UserDAO userDAO;
    @Resource
    private AuthenticationManager authenticationManagerBean;
    @Resource
    private JwtProvider jwtProvider;
    @Resource
    private RoleService roleService;

    @Override
    public UserEntity getByUsername(String username) {
        return userDAO.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format(USER_WITH_USERNAME_NOT_FOUND, username)));
    }

    @Override
    public JwtResponse authenticate(String username, String password) {
        Authentication authentication = authenticationManagerBean.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));

        SecurityContextHolder.getContext()
                .setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserPrinciple userPrinciple = (UserPrinciple) authentication.getPrincipal();
        return new JwtResponse(jwt, userPrinciple.getUserEntity());
    }

    @Transactional
    @Override
    public void saveUser(UserEntity user) {
        if (!userDAO.findByUsername(user.getUsername())
                .isPresent()) {
            userDAO.save(user);
        } else {
            throw new FlashCardsException(
                    String.format(USERNAME_ALREADY_IN_USE,
                                  user.getUserId(),
                                  user.getUsername())
            );
        }
    }

    @Override
    public void saveUser(UserEntity user, boolean isUser) {
        if (isUser) {
            user.setRole(roleService.getRoleByName(RoleEnum.ROLE_USER.name()));
        }
        saveUser(user);
    }

    @Override
    public void updateUser(UserEntity user) {
        Optional<UserEntity> existingUser = userDAO.findByUsername(user.getUsername());
        if (existingUser.isPresent() && user.getUserId() != existingUser.get().getUserId()) {
            throw new InvalidUsernameException(USERNAME_ALREADY_IN_USE);
        }
        if (!userDAO.findById(user.getUserId()).isPresent()) {
            throw new UserNotFoundException(
                    String.format(CANNOT_UPDATE_USER_NOT_FOUND_BY_ID,
                                  user.getUserId(),
                                  user.getUserId())
            );
        }
        updateUserInternal(user);
    }

    private void updateUserInternal(UserEntity user) {
        if (StringUtils.isEmpty(user.getPassword())) {
            userDAO.updateUser(user.getUserId(), user.getFirstName(), user.getLastName(),
                               user.getUsername(),user.getRole());
        } else {
            userDAO.updateUser(user.getUserId(), user.getFirstName(), user.getLastName(),
                               user.getUsername(), user.getPassword(), user.getRole());
        }
    }
}
