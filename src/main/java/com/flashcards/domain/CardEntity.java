package com.flashcards.domain;

import javax.persistence.*;

@Entity
@Table(name = "card", schema = "flashcards")
public class CardEntity {

    private int cardId;
    private String text;
    private String image;
    private String audio;
    private CategoryEntity category;
    private LevelEntity level;
    private UserEntity user;

    public CardEntity(int cardId) {
        this.cardId = cardId;
    }

    public CardEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id")
    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Basic
    @Column(name = "audio")
    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    @Basic
    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "category_id")
    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    @Basic
    @ManyToOne
    @JoinColumn(name = "level_id", referencedColumnName = "level_id")
    public LevelEntity getLevel() {
        return level;
    }

    public void setLevel(LevelEntity level) {
        this.level = level;
    }

    @Basic
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
