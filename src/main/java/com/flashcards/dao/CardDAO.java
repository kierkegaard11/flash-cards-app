package com.flashcards.dao;

import com.flashcards.domain.CardEntity;
import com.flashcards.domain.CategoryEntity;
import com.flashcards.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardDAO extends JpaRepository<CardEntity, Integer> {

    @Query("SELECT c FROM CardEntity c WHERE c.category =?1")
    List<CardEntity> getAllCardsByCategoryId(CategoryEntity category);

    @Query("SELECT c FROM CardEntity c WHERE c.level.levelId = ?1 AND c.user = ?2")
    List<CardEntity> getAllCardsByLevel(int level, UserEntity user);
}
