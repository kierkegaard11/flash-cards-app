package com.flashcards.service;

import com.flashcards.domain.CardChildAggregateEntity;
import com.flashcards.domain.CardChildAggregateId;
import com.flashcards.domain.CategoryEntity;

import java.util.List;

public interface CardChildAggregateService {
    void saveCardChildAggregate(CardChildAggregateId cardChildAggregate);

    void addNewAggregate(int cardId, CategoryEntity category);

    void deleteAggregate(int cardId);

    List<CardChildAggregateEntity> getChosenCardsByChildId(int childId);
}
