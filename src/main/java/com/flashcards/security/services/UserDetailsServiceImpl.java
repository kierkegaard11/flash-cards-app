package com.flashcards.security.services;

import com.flashcards.domain.UserEntity;
import com.flashcards.service.UserService;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private UserService userServiceImpl;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserEntity user = Optional.ofNullable(userServiceImpl.getByUsername(username))
                .orElseThrow(
                        () -> new UsernameNotFoundException("User Not Found with -> username : " + username));

        return UserPrinciple.build(user);
    }
}
