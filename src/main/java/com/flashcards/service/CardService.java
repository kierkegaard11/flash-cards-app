package com.flashcards.service;

import com.flashcards.domain.CardEntity;
import com.flashcards.domain.UserEntity;

import java.util.Arrays;
import java.util.List;

public interface CardService {

    List<CardEntity> getCardsByCategoryId(int categoryId);

    List<CardEntity> getCardsByLevel(int level, UserEntity user);

    CardEntity saveCard(CardEntity mappedCard);

    void deleteCard(int cardId);
}
