package com.flashcards.service.impl;

import com.flashcards.service.PasswordResetTokenService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {

    @Override
    public String createPasswordResetToken() {
        return UUID.randomUUID().toString();
    }

}
