package com.flashcards.service.impl;

import com.flashcards.dao.CardChildAggregateDAO;
import com.flashcards.domain.*;
import com.flashcards.service.CardChildAggregateService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CardChildAggregateServiceImpl implements CardChildAggregateService {

    @Resource
    private CardChildAggregateDAO cardChildAggregateDAO;

    @Override
    public void saveCardChildAggregate(CardChildAggregateId cardChildAggregate) {
        CardEntity cardEntity = new CardEntity(cardChildAggregate.getCardId());
        ChildEntity childEntity = new ChildEntity(cardChildAggregate.getChildId());
        CardChildAggregateEntity cardChildAggregateEntity = new CardChildAggregateEntity();
        cardChildAggregateEntity.setCardChildAggregateId(cardChildAggregate);
        cardChildAggregateEntity.setCardEntity(cardEntity);
        cardChildAggregateEntity.setChildEntity(childEntity);
        cardChildAggregateEntity.setState(0);
        cardChildAggregateDAO.save(cardChildAggregateEntity);
    }

    @Override
    public void addNewAggregate(int cardId, CategoryEntity category) {
        List<CardChildAggregateEntity> chosenCards = cardChildAggregateDAO.getByCategory(category.getCategoryId());
        for(CardChildAggregateEntity c : chosenCards){
            CardChildAggregateEntity cardChildAggregateEntity = new CardChildAggregateEntity();
            CardEntity cardEntity = new CardEntity(cardId);
            cardChildAggregateEntity.setCardChildAggregateId(new CardChildAggregateId(cardId, c.getChildEntity().getChildId()));
            cardChildAggregateEntity.setCardEntity(cardEntity);
            cardChildAggregateEntity.setChildEntity(c.getChildEntity());
            cardChildAggregateEntity.setState(0);
            cardChildAggregateDAO.save(cardChildAggregateEntity);
        }
    }

    @Override
    public void deleteAggregate(int cardId) {

    }

    @Override
    public List<CardChildAggregateEntity> getChosenCardsByChildId(int childId) {
        return cardChildAggregateDAO.getByChildId(childId);
    }

}
