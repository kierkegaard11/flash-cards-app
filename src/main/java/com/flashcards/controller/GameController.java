package com.flashcards.controller;

import com.flashcards.domain.CustomResponseEntity;
import com.flashcards.dto.CardDTO;
import com.flashcards.dto.CategoryDTO;
import com.flashcards.dto.ChosenCategoryDTO;
import com.flashcards.exception.FlashCardsException;
import com.flashcards.facade.GameFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/game")
@PreAuthorize("hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_USER) " +
        "|| hasRole(T(com.flashcards.controller.ControllerConstants).ROLE_EXPERT)")
public class GameController {

    private static final Logger LOG = LoggerFactory.getLogger(GameController.class);
    private static final String SAVE_CHOSEN_CATEGORY_ERROR = "Error while saving chosen category!";
    private static final String GET_CARDS_FOR_GAME_ERROR = "Error while retreiving cards for game!";
    private static final String SAVE_CHOSEN_CATEGORY_SUCCESS = "Chosen category saved successfully!";
    @Autowired
    private GameFacade gameFacade;

    @PostMapping("/stagodbukvalno")
    ResponseEntity<?> saveChosenCategory(@RequestBody ChosenCategoryDTO chosenCategoryDTO) {
        try {
            // TODO - add validator for user access
            gameFacade.saveChosenCategory(chosenCategoryDTO);
            return CustomResponseEntity.ok(SAVE_CHOSEN_CATEGORY_SUCCESS);
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(SAVE_CHOSEN_CATEGORY_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(SAVE_CHOSEN_CATEGORY_ERROR);
        }

    }

    @GetMapping("/words")
    ResponseEntity<?> startWords(@RequestParam int childId){
        try {
            // TODO - add validator for user access
            List<CardDTO> cards = gameFacade.getCardsForGame(childId);
            return CustomResponseEntity.ok(cards);
        } catch (FlashCardsException e) {
            LOG.warn(e.getMessage(), e);
            return CustomResponseEntity.error(SAVE_CHOSEN_CATEGORY_ERROR);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(SAVE_CHOSEN_CATEGORY_ERROR);
        }

    }

}
