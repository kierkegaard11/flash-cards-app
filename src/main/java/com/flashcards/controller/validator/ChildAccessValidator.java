package com.flashcards.controller.validator;

import com.flashcards.service.ChildService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ChildAccessValidator extends CustomAccessValidator {

    @Resource
    private ChildService childService;

    public void isUserAccessingHisChild(int childId) {
    }
}
