package com.flashcards.controller;

public final class ControllerConstants {

    // @PreAuthorize adds "ROLE_" prefix
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_EXPERT = "EXPERT";
    public static final String ROLE_USER = "USER";

    private ControllerConstants() {
    }

}
