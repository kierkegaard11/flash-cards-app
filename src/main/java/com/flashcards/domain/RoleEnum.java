package com.flashcards.domain;

public enum RoleEnum {
    ROLE_USER,
    ROLE_EXPERT,
    ROLE_ADMIN
}
