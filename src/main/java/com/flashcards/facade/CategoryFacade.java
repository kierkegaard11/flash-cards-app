package com.flashcards.facade;

import com.flashcards.dto.CategoryDTO;

import java.util.List;

public interface CategoryFacade {

    List<CategoryDTO> getAllCategories(int userId);

    List<CategoryDTO> searchCategories(String searchCriteria, int userId);

    CategoryDTO saveCategory(CategoryDTO categoryDTO);

    CategoryDTO updateCategory(CategoryDTO categoryDTO, int categoryId);

    void deleteCategory(int categoryId);
}
