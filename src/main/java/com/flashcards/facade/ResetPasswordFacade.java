package com.flashcards.facade;

public interface ResetPasswordFacade {

    void sendResetPasswordEmail(String username);

    void validatePasswordResetToken(int userID, String token);

}
