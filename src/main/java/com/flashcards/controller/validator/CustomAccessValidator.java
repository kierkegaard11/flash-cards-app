package com.flashcards.controller.validator;

import com.flashcards.util.CustomSessionUtil;

public class CustomAccessValidator {

    protected static final String USER_NOT_ACCESSING_HIS_RESOURCES_ERROR = "User tried to access someone else's resources";

    public void isUserAccessingHisResources(int userID) {
        if (CustomSessionUtil.getUserFromSession()
                .getUserId() != userID) {
            throw new RuntimeException(USER_NOT_ACCESSING_HIS_RESOURCES_ERROR);
        }
    }
}
