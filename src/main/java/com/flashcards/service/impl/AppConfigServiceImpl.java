package com.flashcards.service.impl;

import com.flashcards.dao.AppConfigDAO;
import com.flashcards.domain.AppConfigEntity;
import com.flashcards.exception.configException.AppConfigNotFoundException;
import com.flashcards.service.AppConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class AppConfigServiceImpl implements AppConfigService {

    private static final String APP_CONFIG_NOT_FOUND_BY_KEY =
        "App config with a key %s does not exist.";
    private static final String CANNOT_UPDATE_APP_CONFIG_NOT_FOUND_BY_ID =
        "Cannot update the app config (%s): " + APP_CONFIG_NOT_FOUND_BY_KEY;

    @Resource
    private AppConfigDAO appConfigDAO;

    @Override
    public List<AppConfigEntity> getAllAppConfig() {
        return appConfigDAO.findAll();
    }

    @Override
    public AppConfigEntity getAppConfigByKey(String key) {
        return appConfigDAO.findById(key).orElseThrow(
            () -> new AppConfigNotFoundException(String.format(APP_CONFIG_NOT_FOUND_BY_KEY, key))
        );
    }

    @Override
    public void updateAppConfig(AppConfigEntity appConfigEntity) {
        Optional<AppConfigEntity> config = appConfigDAO.findById(appConfigEntity.getKey());
        if (!config.isPresent()) {
            throw new AppConfigNotFoundException(
                String.format(CANNOT_UPDATE_APP_CONFIG_NOT_FOUND_BY_ID,
                              appConfigEntity.getKey(),
                              appConfigEntity.getKey())
            );
        }
        appConfigDAO.updateConfig(appConfigEntity.getKey(), appConfigEntity.getValue());
    }
}
