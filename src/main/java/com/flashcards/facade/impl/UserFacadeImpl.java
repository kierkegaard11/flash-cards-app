package com.flashcards.facade.impl;

import com.flashcards.domain.JwtResponse;
import com.flashcards.domain.UserEntity;
import com.flashcards.dto.JwtResponseDTO;
import com.flashcards.dto.LoginDTO;
import com.flashcards.dto.UserInfoDTO;
import com.flashcards.facade.UserFacade;
import com.flashcards.service.ModelMapperService;
import com.flashcards.service.PropertyMapConfigurationService;
import com.flashcards.service.ResetPasswordService;
import com.flashcards.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UserFacadeImpl implements UserFacade {

    @Resource
    public UserService userService;
    @Resource
    private ModelMapperService modelMapperService;
    @Resource
    private ResetPasswordService resetPasswordService;
    @Resource
    private PasswordEncoder encoder;
    @Resource
    private PropertyMapConfigurationService propertyMapConfigurationRegisterServiceImpl;

    @Override
    public JwtResponseDTO authenticate(LoginDTO loginDTO) {
        JwtResponse jwtResponse = userService.authenticate(loginDTO.getUsername(), loginDTO.getPassword());
        return modelMapperService.getModelMapper()
                .map(jwtResponse, JwtResponseDTO.class);
    }

    @Override
    public void createUser(UserInfoDTO userInfoDTO) {
        UserEntity userEntity = createUserInternal(userInfoDTO);
        userService.saveUser(userEntity);
    }

    @Override
    public void createUserAsUser(UserInfoDTO userInfoDTO) {
        UserEntity userEntity = createUserInternal(userInfoDTO);
        userService.saveUser(userEntity, true);
    }

    @Override
    public void changePassword(String token, String newPassword) {
        UserEntity userEntity = resetPasswordService.getPasswordResetTokenByToken(token)
                .getUser();
        userEntity.setPassword(encoder.encode(newPassword));
        userService.updateUser(userEntity);
        resetPasswordService.deletePasswordResetTokenByUserId(userEntity.getUserId());
    }

    private UserEntity createUserInternal(UserInfoDTO userInfoDTO) {
        userInfoDTO.setPassword(encoder.encode(userInfoDTO.getPassword()));
        ModelMapper modelMapper = modelMapperService.getConfiguredModelMapper(
                propertyMapConfigurationRegisterServiceImpl.getConfiguration());
        UserEntity userEntity = modelMapper.map(userInfoDTO, UserEntity.class);
        userEntity.setUserId(0);
        return userEntity;
    }
}
