package com.flashcards.service.impl;

import com.flashcards.dao.CardDAO;
import com.flashcards.domain.CardEntity;
import com.flashcards.domain.CategoryEntity;
import com.flashcards.domain.UserEntity;
import com.flashcards.exception.FlashCardsException;
import com.flashcards.service.CardService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    private static final String CARD_NOT_FOUNT_BY_GIVEN_ID = "Card not found by given id!";
    @Resource
    private CardDAO cardDAO;

    @Override
    public List<CardEntity> getCardsByCategoryId(int categoryId) {
        return cardDAO.getAllCardsByCategoryId(new CategoryEntity(categoryId));
    }

    @Override
    public List<CardEntity> getCardsByLevel(int level, UserEntity user) {
        return cardDAO.getAllCardsByLevel(level, user);
    }

    @Override
    public CardEntity saveCard(CardEntity card) {
        return cardDAO.save(card);
    }

    @Override
    public void deleteCard(int cardId) {
        if (cardDAO.findById(cardId).isPresent()) {
            cardDAO.deleteById(cardId);
        } else {
            throw new FlashCardsException(CARD_NOT_FOUNT_BY_GIVEN_ID);
        }
    }
}
