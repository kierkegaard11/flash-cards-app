package com.flashcards.dao;

import com.flashcards.domain.PasswordResetTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Repository
public interface ResetPasswordDAO extends JpaRepository<PasswordResetTokenEntity, Integer> {

    @Query("SELECT prt FROM PasswordResetTokenEntity prt WHERE prt.token = ?1")
    Optional<PasswordResetTokenEntity> findByToken(String token);

    @Query("SELECT prt FROM PasswordResetTokenEntity prt WHERE prt.user.userId = ?1")
    Optional<PasswordResetTokenEntity> findByUserId(int userID);

    @Transactional
    @Modifying
    @Query("UPDATE PasswordResetTokenEntity prt SET prt.token = ?2, prt.expiryDate = ?3 WHERE prt.passwordResetTokenId = ?1")
    void update(int passwordResetTokenID, String token, Date expiryDate);

    @Transactional
    @Modifying
    @Query("DELETE FROM PasswordResetTokenEntity prt WHERE prt.user.userId = ?1")
    void deleteByUserId(int userID);

}
