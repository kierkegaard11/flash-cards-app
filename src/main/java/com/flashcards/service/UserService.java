package com.flashcards.service;

import com.flashcards.domain.JwtResponse;
import com.flashcards.domain.UserEntity;

public interface UserService {

    UserEntity getByUsername(String username);

    JwtResponse authenticate(String username, String password);

    void saveUser(UserEntity user);

    void saveUser(UserEntity user, boolean isUser);

    void updateUser(UserEntity userEntity);
}
